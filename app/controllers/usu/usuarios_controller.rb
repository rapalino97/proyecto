module Usu
    class UsuariosController < ApplicationController

        def index
            usuario = Usuario.order('id');
            if usuario.empty?
                render json: {
                status: '204',
                message: 'No hay usuarios creados',
                }, status: :ok
            else 
                render json: {
                status: '200',
                message: 'Los Usuarios han sido cargadas exitosamente',
                data: usuario
                }, status: :ok
            end
        end

        def create
            usuario = Usuario.new(usuario_params)
            if usuario.save
                render json: {
                status: '200',
                message: 'El Usuario fue creado exitosamente',
                data: usuario
                }, status: :ok
            else
                render json: {
                status: '400',
                message: 'La tarea no pudo ser creada',
                data: usuario.errors
                }, status: :unprocessable_entity
            end
        end

        private
        def usuario_params
            params.permit(:nombre, :usuario)
        end


    end
end