module Api
    #Controlador debe tener nombre plural
    class TareasController < ApplicationController

        def index
            tareas = Tarea.order('id');
            if tareas.empty?
                render json: {
                status: '204',
                message: 'No hay tareas creadas',
                }, status: :ok
            else 
                render json: {
                status: '200',
                message: 'Las Tareas han sido cargadas exitosamente',
                data: tareas
                }, status: :ok
            end
        end

        #
        def show
            tareas = Tarea.find_by_id(params[:id]);
            if tareas
                render json: {
                status: '200',
                message: 'La tarea ha sido cargada exitosamente',
                data: tareas
                }, status: :ok
            else
                render json: {
                status: '204',
                message: 'La tarea no ha sido encontrada',
                }, status: :ok
            end
        end

        def create
            tarea = Tarea.new(tarea_params)
            if tarea.save
                render json: {
                status: '200',
                message: 'La tarea fue creada exitosamente',
                data: tarea
                }, status: :ok
            else
                render json: {
                status: '400',
                message: 'La tarea no pudo ser creada',
                data: tarea.errors
                }, status: :unprocessable_entity
            end
        end

        def destroy(estado="")
            tarea = Tarea.find_by_id(params[:id])
            if tarea
                if tarea.destroy
                    if estado == "TERMINADA"
                        estado = 'Terminada y '
                    else
                        estado = ''
                    end 
                    render json: {
                    status: '200',
                    message: 'La tarea fue ' + estado + 'Eliminada exitosamente',
                    }, status: :ok
                else
                    render json: {
                    status: '400',
                    message: 'La tarea no pudo ser eliminada',
                    data: tarea.errors
                    }, status: :unproccessable_entity
                end
            else
                render json: {
                status: '400',
                message: 'La tarea no ha sido encontrada y no se puede eliminar',
                }, status: :ok
            end

        end

        def update
            tarea = Tarea.find(params[:id])
            if tarea_params[:estado] == "TERMINADA"
                estado = "TERMINADA"
                destroy(estado)
            else
                if tarea.update(tarea_params)
                    render json: {
                    status: '200',
                    message: 'Tarea fue editada exitosamente',
                    data: tarea
                    }, status: :ok
                else
                    render json: {
                    status: '400',
                    message: 'La tarea no pudo ser editada',
                    data: tarea.errors
                    }, status: :unproccessable_entity
                end
            end
            
        end

        private
        def tarea_params
            params.permit(:nombre, :descripcion, :estado)
        end


    end
end